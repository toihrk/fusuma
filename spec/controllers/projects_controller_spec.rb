require 'spec_helper'

describe ProjectsController do

  def valid_attributes
    {}
  end

  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all projects as @projects" do
      project = Project.new(valid_attributes).save
      get :index, {}, valid_session
      assigns(:projects).should eq([project])
    end
  end

  describe "GET show" do
    it "assigns the requested project as @project" do
      project = Project.new(valid_attributes).save
      get :show, {:id => project.to_param}, valid_session
      assigns(:project).should eq(project)
    end
  end

  describe "GET new" do
    it "assigns a new project as @project" do
      get :new, {}, valid_session
      assigns(:project).should be_a_new(Project)
    end
  end

  describe "GET edit" do
    it "assigns the requested project as @project" do
      project = Project.new(valid_attributes).save
      get :edit, {:id => project.to_param}, valid_session
      assigns(:project).should be(project)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "create a new Project" do
        expext {
          post :create, {:task => valid_attributes}, valid_session
        }.to change(Project, :count).by(1)
      end

      it "assigns a newly created project as @project"

    end
  end
end

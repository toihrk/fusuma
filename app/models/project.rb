# -*- coding: utf-8 -*-
class Project

  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,   :type => String
  field :slide,  :type => String,  :default => <<EOS
<section id="home" class="step">
  <h1>Hello!</h1>
  <p>This is sample.</p>
</section>
<section class="step" data-x="1000">
   <h1>You can use the following HTML tags.</h1>
   <p>
    <code>&lt;section&gt;</code>
    <code> &lt;h1&gt;</code>
    <code> &lt;h2&gt;</code>
    <code> &lt;h3&gt;</code>
    <code> &lt;p&gt;</code>
    <code> &lt;img&gt;</code>
    <code> &lt;code&gt;</code>
    <code> &lt;b&gt;</code>
    <code> &lt;ul&gt;</code>
    <code> &lt;ol&gt;</code>
    <code> &lt;li&gt;</code>
    <code> &lt;i&gt;</code>
    <code> &lt;a&gt;</code>
    <code> &lt;br /&gt;</code>
  </p>
  <h2>You can use Twitter Bootstrap BaseCSS</h2>
  <h3>Typography</h3>
  <h1>h1. Heading 1</h1>
  <h2>h2. Heading 2</h2>
  <h3>h3. Heading 3</h3>
  <h3>Buttons</h3>
  <a class="btn">Button</a>
  <a class="btn btn-primary">Button</a>
  <a class="btn btn-info">Button</a>
  <a class="btn btn-success">Button</a>
  <a class="btn btn-warning">Button</a>
  <a class="btn btn-danger">Button</a>
  <a class="btn btn-inverse">Button</a>
  <h3>And more...</h3>
</section>

<section class="step" data-x="2000">
  <h1>Thanks to these products.</h1>
  <h2>Ruby on Rails</h2>
  <h2>Jmpress.js</h2>
  <h2>Twitter Bootstrap</h2>
  <h2>and so on!</h2>
  
</section>
<section class="step" data-x="3000">
  <p>いや、がんばって英語で書いたけど厳しいなコレ。</p>
  <p><a href="http://www.kosen-venture.com/">高専ベンチャープロジェクト</a>で作っていたものを作り直しました。</p>
  <p>本当だったら、<a href="http://twitter.com/neo6120">じぐそうさん</a>のJavascriptでもっとリッチなエディタになるはずだったんだけど、ボクの力不足で組み込めませんでした。</p>
  <p>ごめんないさい。</p>
  <p>てか、こんな手抜きな感じでごめんないさい。</p>
  <p><a href="http://twitter.com/Whimsical_Cat">変態猫先輩</a>のアートワークとかも組み込めませんでした。</p>
  <p>ごめんないさい。</p>
  <p>本当にごめんないさい。</p>
  <p>でも、あの合宿を経験できとてもよかったです。</p>
  <p>とってもよいチームで多くの時間を過ごすことができてよかったです。</p>
  <p>ナニワトモアレ、許してください。</p>
　<p> 2013年4月 <a href="http://twitter.com/toihrk">とい</a></p>
</section>
EOS

  field :public, :type => Boolean, :default => true

  belongs_to :user

  validates_presence_of :name
  validates_presence_of :slide

  before_save :sanitize_slide
  before_create :sanitize_slide

  after_create :attachment_user

  def sanitize_slide_for_preview
    sanitize_slide
  end

  private
  def sanitize_slide
    self.slide = Sanitize.clean(self.slide, 
                                :elements => ['section', 'h1', 'h2', 'h3', 'p', 'img', 
                                              'code', 'br', 'b', 'ul', 'ol','li', 'i', 'a'],
                                :attributes => {'section' => ['class', 'id', 'data-x'],
                                  'h1' => ['class'],
                                  'h2' => ['class'],
                                  'h3' => ['class'],
                                  'p'  => ['class'],
                                  'img' => ['src', 'alt', 'height', 'width', 'class'],
                                  'code' => ['class'],
                                  'b' => ['class'],
                                  'ul' => ['class'],
                                  'ol' => ['class'],
                                  'li' => ['class'],
                                  'i' => ['class'],
                                  'a' => ['href', 'title', 'target', 'class']})
  end

  def attachment_user
    self.user.projects << self
    self.user.save
  end
end

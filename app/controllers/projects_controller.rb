class ProjectsController < ApplicationController
  before_filter :authenticate_user!, :except => %w(show trial preview)
  layout 'slideshow', :only => %w(show preview)

  def index
    @projects = Project.all
  end

  def new
    @project = Project.new(:user => current_user)
  end

  def edit
    @project = Project.find(params[:id])
  end

  def show
    @project = Project.find(params[:id])
  end

  def trial
    @project = Project.new
  end

  def preview
    @project = Project.new
    @project.slide = params[:slide]
    @project.sanitize_slide_for_preview
    render :html => @project.slide, :action => :show
  end

  def source
    @project = Project.find(params[:id])
  end
    
  def create
    @project = Project.new(params[:project])
    @project.user = current_user
    if @project.save
      redirect_to user_project_path(:user_id => current_user, :id => @project.id)
    else
      render :action => :new
    end
  end

  def update
    @project = Project.find(params[:id])
    @project.attributes = params[:project]
    if @project.save
      redirect_to user_project_path(:user_id => current_user, :id => @project.id)
    else
      render :action => :edit
    end
  end

  def destroy
    @project = Project.find(params[:id])
    if @project.delete
      redirect_to user_projects_path(current_user.id)
    end
  end
end



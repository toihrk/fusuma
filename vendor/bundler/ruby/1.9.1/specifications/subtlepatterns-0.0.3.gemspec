# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "subtlepatterns"
  s.version = "0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Masahiro Saito"]
  s.date = "2013-01-08"
  s.description = "gem for http://subtlepatterns.com"
  s.email = ["camelmasa@gmail.com"]
  s.homepage = "https://github.com/camelmasa/subtlepatterns"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "gem for http://subtlepatterns.com"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, ["~> 3.2.9"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
    else
      s.add_dependency(%q<rails>, ["~> 3.2.9"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, ["~> 3.2.9"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
  end
end
